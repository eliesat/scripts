#!/bin/sh
echo "> cleaning up please wait..."
sleep 3s 

if command -v dpkg &> /dev/null; then
apt-get -remove enigma2-plugin-extensions-eliesatpanel
apt-get -remove enigma2-plugin-extensions-ajpanel
else
opkg remove enigma2-plugin-extensions-eliesatpanel
opkg remove enigma2-plugin-extensions-ajpanel
fi

if [ -d /usr/lib/enigma2/python/Plugins/Extensions/AJPan ]; then
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/AJPan > /dev/null 2>&1
fi

rm -rf /var/cache/opkg/*
rm -rf /var/lib/opkg/lists/*
rm -rf /run/opkg.lock
rm -r /var/volatile/cache/opkg >/dev/null 2>&1

find /var/lib/opkg/info -type f -name "*.ajpanel" -exec rm -f {} \; >/dev/null 2>&1
find /var/lib/opkg/info -type f -name "*.eliesatpanel" -exec rm -f {} \; >/dev/null 2>&1

echo "> done, your device will reboot now please wait..."
reboot 

exit 0

