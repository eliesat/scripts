#!/bin/bash

#configuration
#########################################
image="openeight"
version="6.8"
device=$(head -n 1 /etc/hostname)

#determine site image name and url based on device name
#########################################
case $device in
novaler4k|multibox4k|zgemmah7|h7)
site="https://www.novaler.com/downloads/enigma2/multibox-4k-ultra-hd";;
novaler4kse|multibox4kse)
site="https://www.novaler.com/downloads/enigma2/multibox-4k-se-ultra-hd";;
novaler4kpro|multibox4kpro)
site="https://www.novaler.com/downloads/enigma2/multibox-4k-pro-ultra-hd";;
*) echo "> your device is not supported "
exit 1 ;;
esac
sleep 3

imgnm=$(curl -s "$site" |  grep "$image"-"$version" | head -n 1 | sed "s/^.*"$image"/"$image"/" | sed 's/".*//')
echo "> "$imgnm" image found ..."
sleep 3

case $device in
novaler4k|multibox4k|zgemmah7|h7)
url=https://www.novaler.com/uploads/enigma/$image/$imgnm;;
novaler4kse|multibox4kse)
url=https://novaler.com/uploads/enigmase/$image/$imgnm;;
novaler4kpro|multibox4kpro)
url=https://novaler.com/uploads/enigmapro/$image/$imgnm;;
*) echo "> your device is not supported "
exit 1 ;;
esac
echo "> $url"
sleep 3

#check mounted storage
#########################################
for ms in "/media/hdd" "/media/usb" "/media/mmc"
do
    if mount|grep $ms >/dev/null 2>&1; then
    echo "> Mounted storage found at: $ms"
    mkdir "$ms"/images >/dev/null 2>&1
    break
    fi
done

if [ -z "$ms" ]; then
echo "> Mount your external memory and try again"
exit 1
fi
sleep 3

#download image to mounted storage
#########################################
echo '> Downloading '$image'-'$version' image to '$ms'/images please wait...'
sleep 3

if wget -q --method=HEAD $url;
 then
wget --show-progress -qO $ms/images/$imgnm $url
else
echo "> check your internet connection and try again or your device is not supported..."
exit 1
fi

#copy image to multiboot upload folders
#########################################
for dir in "/media/hdd/ImagesUpload/" "/media/hdd/open-multiboot-upload/" "/media/hdd/OPDBootUpload/" "/media/hdd/EgamiBootUpload/"
do
if [ -d $dir ] ; then
echo "> "$dir" folder found ..."
sleep 1
echo "> copying image to "$dir" folder please wait ..."
sleep 1
cp $ms/images/$imgnm $dir >/dev/null 2>&1
fi
done
echo '> Eliesat enjoy... '