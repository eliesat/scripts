#wget -O - -q https://feeds2.mynonpublic.com/devel-feed | bash

#!/bin/bash

MODEL=$(grep ^machinebuild=* /usr/lib/enigma.info | sed 's/machinebuild=//g' | tr -d "'")
OLD=$(grep ^model=* /usr/lib/enigma.info | sed 's/model=//g' | tr -d "'")
OE=$(grep ^oe=* /usr/lib/enigma.info | sed 's/oe=//g' | tr -d "'")
#VERSION=$(grep ^imageversion=* /usr/lib/enigma.info | sed 's/imageversion=//g')
#VERSION=$(sed -e "s/^'//" -e "s/'$//" <<<"$VERSION")

if [ "$OE" == "OE-Alliance 5.4" ]; then
    VERSION=7.4
fi

if [ "$OE" == "OE-Alliance 5.3" ]; then
    VERSION=7.3
fi

if [ "$OE" == "OE-Alliance 5.2" ]; then
    VERSION=7.2
fi

export D=${D}

rm $D/etc/opkg/devel-$MODEL-feed.conf >/dev/null 2>&1 || true
rm $D/etc/opkg/devel-$OLD-feed.conf >/dev/null 2>&1 || true
rm $D/etc/opkg/preview-$MODEL-feed.conf >/dev/null 2>&1 || true
echo src/gz openatv-devel-$MODEL https://feeds2.mynonpublic.com/$VERSION/devel/devel-$MODEL > $D/etc/opkg/devel-$MODEL-feed.conf
opkg update
opkg upgrade
echo " "
echo "openATV Enigma2 Devel Feed succesfully installed"
echo " "
exit 0
