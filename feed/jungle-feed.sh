#!/bin/bash

#remove feed
if [ -f /etc/opkg/jungle-feed.conf ]; then

echo "> Removing Jungle Feed Please Wait ..."
sleep 3
rm -rf /etc/opkg/jungle-feed.conf > /dev/null 2>&1
rm -rf /var/lib/opkg/lists/* > /dev/null 2>&1
opkg update

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Eliesat          *"
echo "*******************************************"
sleep 3s

else
echo "> Installing Jungle Feed Please Wait ..."
sleep 3

echo "src/gz jungle-feed http://tropical.jungle-team.online/oasis/H2O" >>/etc/opkg/jungle-feed.conf

opkg update
echo " "
sleep 3s
echo "> Jungle feed installed successfully"
sleep 3s

fi
exit 0