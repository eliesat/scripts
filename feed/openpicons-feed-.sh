#!/bin/bash

left=">>>>"
right="<<<<"
LINE1="---------------------------------------------------------"
LINE2="-------------------------------------------------------------------------------------"

if [ -f /etc/opkg/snp-feed.conf ]; then    

rm -rf /etc/opkg/snp-feed.conf
rm -rf /etc/opkg/srp-feed.conf

    echo "$LINE1"
    echo "> Openpicons Feed Removed Successfully"
    echo "$LINE1"
    sleep 3s

rm -rf /var/cache/opkg/*
rm -rf /var/lib/opkg/lists/*

opkg update

else

echo "$LINE1"
echo "> Installing Openpicon Feed Please Wait ..."
echo "$LINE1"
sleep 3

echo "src/gz snp-feed https://openpicons.com/picons/full-motor-snp/ipk" >>/etc/opkg/snp-feed.conf

echo "src/gz srp-feed https://openpicons.com/picons/full-motor-srp/ipk" >>/etc/opkg/srp-feed.conf

    echo "$LINE1"
    echo "> Openpicons Feed Installed Successfully"
    echo "$LINE1"
    sleep 3s

rm -rf /var/cache/opkg/*
rm -rf /var/lib/opkg/lists/*

opkg update

echo "
        ${left}         ELIESAT    ${right}"
echo "$LINE1"
sleep 3
init 3

fi
exit 0